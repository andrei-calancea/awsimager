# IMAGER

# Installation and processing
1. Copy the `.env.example` file to `.env` and change it to suit your needs;
2. Change the section `database` of the `app/config.php` according to your data;
3. Create dependencies by running `composer install` in console;
4. Run the migrations using Phalcon dev tools running the command `phalcon migration --action=run`
5. Create the necessary models by running `phalcon create-all-models`
6. Compare the list of files from data base to the list of files from your AWS s3 bucket by running `php app/cli.php aws missingImages`
7. Check the `var/logs/aws-missing-files.log` file for the results of the step 6 operation.