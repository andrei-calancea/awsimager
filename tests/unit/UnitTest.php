<?php

/**
 * Class UnitTest
 */
class UnitTest extends \PHPUnit_Framework_TestCase
{
    public function testEarthEqualsEarth()
    {
        $this->assertEquals(
            "earth",
            "earth",
            "Passed test"
        );

        $this->assertEquals(
            "earth",
            "universe",
            "NOT passed test"
        );
    }

    public function testTrueAssertsToTrue()
    {
        $this->assertTrue(true);
    }
}