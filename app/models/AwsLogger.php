<?php

class AwsLogger extends Logger
{
    /**
     * @param $fileNames
     */
    public static function logMissingFiles($fileNames)
    {
        if (is_array($fileNames)) {
            $fileNames = json_encode($fileNames);
        } elseif (!is_string($fileNames)) {
            $fileNames = 'Invalid file names!';
        }

        $path = str_replace("\\", "/", self::getPath() . '/aws-missing-files.log');

        $f = fopen($path, 'a');
        fwrite($f, date('m-d-Y H:i:s') . " " . $fileNames . "\n");
        fclose($f);
    }
}