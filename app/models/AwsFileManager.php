<?php

/**
 * Class AwsFileManager
 */
class AwsFileManager
{
    /** @var \Aws\S3\S3Client */
    protected $s3Client;

    /**
     * AwsFileManager constructor.
     * @param \Aws\S3\S3Client $s3Client
     */
    public function __construct(\Aws\S3\S3Client $s3Client)
    {
        $this->s3Client = $s3Client;
    }

    /**
     * @param null $bucketName
     * @return array
     */
    public function getAllFiles($bucketName = null)
    {
        $files = [];
        $iterator = $this->s3Client->getIterator('ListObjects', array(
            'Bucket' => $bucketName ?? getenv('AWS_BUCKET_NAME'),
            'Delimiter' => '.DS_Store'
        ));

        foreach ($iterator as $object) {
            $files[] = Tools::extractFileName($object['Key']);
        }

        return $files;
    }

    public function missingInDB()
    {
        $bucketFiles = $this->getAllFiles();

        $imageNames = ImageStore::getImageNames();
        $results = array_filter($bucketFiles, function ($v) use ($imageNames) {
            return !in_array($v, $imageNames);
        });

        $thumbnailNames = ThumbnailStore::getThumbnailNames();
        $results = array_filter($results, function ($v) use ($thumbnailNames) {
            return !in_array($v, $thumbnailNames);
        });

        return $results;
    }
}