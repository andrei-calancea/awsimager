<?php

/**
 * Class Logger
 */
class Logger
{
    /**
     * @return string
     */
    public static function getPath()
    {
        if (!is_file(LOG_PATH)) {
            mkdir(LOG_PATH, 0664, true);
        }
        return LOG_PATH;
    }
}