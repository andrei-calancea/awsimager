<?php

class Tools
{
    public static function extractFileName($fullPath)
    {
        preg_match('/(.+\/)*(.+\..+)$/', $fullPath, $matches);
        return $matches[2] ?? '';
    }
}