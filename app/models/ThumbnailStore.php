<?php

class ThumbnailStore extends Thumbnail
{
    /**
     * @return array
     */
    public static function getThumbnailNames()
    {
        $thumbnailNames = [];
        $results = self::find([
            'columns' => 'thumbnail_name'
        ]);

        foreach ($results as $result) {
            $thumbnailNames[] = $result->thumbnail_name;
        }
        return $thumbnailNames;
    }
}
