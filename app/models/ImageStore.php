<?php

class ImageStore extends Image
{
    /**
     * @return array
     */
    public static function getImageNames()
    {
        $imageNames = [];
        $results = self::find([
            'columns' => 'image_name'
        ]);

        foreach ($results as $result) {
            $imageNames[] = $result->image_name;
        }
        return $imageNames;
    }
}
