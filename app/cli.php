<?php
use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as ConsoleApp;
use Phalcon\Loader;

define('BASE_PATH', dirname(__DIR__));

// Using the CLI factory default services container
$di = new CliDI();

/**
 * Composer autoload
 */
include BASE_PATH . '/vendor/autoload.php';

/**
 * Environment variables
 */
$dotEnv = Dotenv\Dotenv::createUnsafeImmutable(BASE_PATH);
$dotEnv->load();

/**
 * Read services
 */
include BASE_PATH . '/app/config/cli_services.php';

/**
 * Register the autoloader and tell it to register the tasks directory
 */
$loader = new Loader();
$loader->registerDirs(
    [
        __DIR__ . '/tasks',
        __DIR__ . '/models',
    ]
);
$loader->register();

// Create a console application
$console = new ConsoleApp();

$console->setDI($di);

/**
 * Process the console arguments
 */
$arguments = [];

foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments['task'] = $arg;
    } elseif ($k === 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments['params'][] = $arg;
    }
}

try {
// Handle incoming arguments
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
// Do Phalcon related stuff here
// ..
    fwrite(STDERR, $e->getMessage() . PHP_EOL);
    exit(1);
} catch (\Throwable $throwable) {
    fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
    exit(1);
} catch (\Exception $exception) {
    fwrite(STDERR, $exception->getMessage() . PHP_EOL);
    exit(1);
}