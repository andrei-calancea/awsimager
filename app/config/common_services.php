<?php

use Phalcon\Mvc\Router;
use Phalcon\Mvc\View;

/**@var $di \Phalcon\Di */



/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include BASE_PATH . "/app/config/config.php";
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'charset' => $config->database->charset
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});

/**
 * Register the AWS s3 client
 */
$di->setShared('s3Client', function () {
    return new \Aws\S3\S3Client([
        'version' => 'latest',
        'region' => getenv('AWS_REGION'),
        'credentials' => [
            'key' => getenv('AWS_ACCESS_KEY_ID'),
            'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
        ],
    ]);
});

$di->setShared('AwsFileManager', function(){
    return new AwsFileManager($this->getS3Client());
});
