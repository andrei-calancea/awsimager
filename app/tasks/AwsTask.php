<?php

use Phalcon\Cli\Task;

class AwsTask extends Task
{
    public function missingImagesAction()
    {
        $AwsFileManager = $this->di->get('AwsFileManager');
        $missingFiles = json_encode($AwsFileManager->missingInDB());
        AwsLogger::logMissingFiles($missingFiles);
        printf("Missing files: %s", $missingFiles);
    }
}